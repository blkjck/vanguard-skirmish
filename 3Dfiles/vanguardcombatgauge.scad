inches = 1;
// set the above to 0 for mm and 1 for inches. setting to 0 does not actually work right now

ifs = 0.4;
//inches side font size

if (inches == 1) 
scale([25.4,25.4,25.4])

	difference()
		{
            {
			cube([10,1,0.1]);
			}
// top heights and markers
		translate([0,0,0.08])
		linear_extrude(height = 0.03)
		union()
			{
			translate([0.3,0.9])
      rotate(270)
				{ 
				text("H1", font="Caladea", size = 0.5);
				}

			translate([1.3,0.9])
			rotate(270)
				{ 
				text("H2", font="Caladea", size = 0.5);
				}

      translate([2.3,0.9])
			rotate(270)
				{ 
				text("H3", font="Caladea", size = 0.5);
				}
        
        translate([3.3,0.9])
        rotate(270)
				{ 
				text("H4", font="Caladea", size = 0.5);
				}
       
        translate([4.3,0.9])
        rotate(270)
				{ 
				text("H5", font="Caladea", size = 0.5);
				}
        
        translate([5.3,0.9])
        rotate(270)
				{ 
				text("H6", font="Caladea", size = 0.5);
				}
        
        translate([6.3,0.9])
        rotate(270)
				{ 
				text("H7", font="Caladea", size = 0.5);
				}
        
        translate([7.3,0.9])
        rotate(270)
				{ 
				text("H8", font="Caladea", size = 0.5);
				}
        
        translate([8.3,0.9])
        rotate(270)
				{ 
				text("H9", font="Caladea", size = 0.5);
				}
        
        translate([9.3,0.98])
        rotate(270)
				{ 
				text("H10", font="Caladea", size = 0.4);
				}
 
			}
        translate([0,0,0.08])
        linear_extrude(height = 0.03)
		union()
		{
		polygon(points=[[1,0],[1.0,0.5],[0.95,0.5]]);
        polygon(points=[[1,1],[1.0,0.5],[0.95,0.5]]);            
		
        polygon(points=[[2,0],[2.0,0.5],[1.95,0.5]]);
        polygon(points=[[2,1],[2.0,0.5],[1.95,0.5]]);
		
        polygon(points=[[3,0],[3.0,0.5],[2.95,0.5]]);
        polygon(points=[[3,1],[3.0,0.5],[2.95,0.5]]);
        
        polygon(points=[[4,0],[4.0,0.5],[3.95,0.5]]);
        polygon(points=[[4,1],[4.0,0.5],[3.95,0.5]]);
		
        polygon(points=[[5,0],[5.0,0.5],[4.95,0.5]]);
        polygon(points=[[5,1],[5.0,0.5],[4.95,0.5]]);
		
        polygon(points=[[6,0],[6.0,0.5],[5.95,0.5]]);
        polygon(points=[[6,1],[6.0,0.5],[5.95,0.5]]);            
		
        polygon(points=[[7,0],[7.0,0.5],[6.95,0.5]]);
        polygon(points=[[7,1],[7.0,0.5],[6.95,0.5]]);
		
        polygon(points=[[8,0],[8.0,0.5],[7.95,0.5]]);
        polygon(points=[[8,1],[8.0,0.5],[7.95,0.5]]);
		
        polygon(points=[[9,0],[9.0,0.5],[8.95,0.5]]);
        polygon(points=[[9,1],[9.0,0.5],[8.95,0.5]]);
		
       polygon(points=[[10,0],[10.0,0.5],[9.95,0.5]]);
       polygon(points=[[10,1],[10.0,0.5],[9.95,0.5]]);
		
//        polygon(points=[[11,0],[11.0,0.5],[10.95,0.5]]);
//        polygon(points=[[11,1],[11.0,0.5],[10.95,0.5]]);
		
//        polygon(points=[[12,0],[12.0,0.5],[11.95,0.5]]);
//        polygon(points=[[12,1],[12.0,0.5],[11.95,0.5]]);
		} 
    
    // bottom inches and markers
		translate([0,0,0])
		linear_extrude(height = 0.03)
		union()
			{
			translate([0.4,0.7])
      rotate([180,0,0])
				{ 
				text("1\"", font="Caladea", size = ifs);
				}

			translate([1.4,0.7])
rotate([180,0,0])				{ 
				text("2\"", font="Caladea", size = ifs);
				}

      translate([2.4,0.7])
rotate([180,0,0])				{ 
				text("3\"", font="Caladea", size = ifs);
				}
        
        translate([3.4,0.7])
rotate([180,0,0])				{ 
				text("4\"", font="Caladea", size = ifs);
				}
       
        translate([4.4,0.7])
rotate([180,0,0])				{ 
				text("5\"", font="Caladea", size = ifs);
				}
        
        translate([5.4,0.7])
rotate([180,0,0])				{ 
				text("6\"", font="Caladea", size = ifs);
				}
        
        translate([6.4,0.7])
rotate([180,0,0])				{ 
				text("7\"", font="Caladea", size = ifs);
				}
        
        translate([7.4,0.7])
rotate([180,0,0])				{ 
				text("8\"", font="Caladea", size = ifs);
				}
        
        translate([8.4,0.7])
rotate([180,0,0])				{ 
				text("9\"", font="Caladea", size = ifs);
				}
 
        translate([9.1,0.7])
rotate([180,0,0])				{ 
				text("10\"", font="Caladea", size = ifs);
				}
 
			}
        translate([0,0,0])
        linear_extrude(height = 0.03)
		union()
		{
		polygon(points=[[1,0],[1.0,0.5],[0.95,0.5]]);
        polygon(points=[[1,1],[1.0,0.5],[0.95,0.5]]);            
		
        polygon(points=[[2,0],[2.0,0.5],[1.95,0.5]]);
        polygon(points=[[2,1],[2.0,0.5],[1.95,0.5]]);
		
        polygon(points=[[3,0],[3.0,0.5],[2.95,0.5]]);
        polygon(points=[[3,1],[3.0,0.5],[2.95,0.5]]);
        
        polygon(points=[[4,0],[4.0,0.5],[3.95,0.5]]);
        polygon(points=[[4,1],[4.0,0.5],[3.95,0.5]]);
		
        polygon(points=[[5,0],[5.0,0.5],[4.95,0.5]]);
        polygon(points=[[5,1],[5.0,0.5],[4.95,0.5]]);
		
        polygon(points=[[6,0],[6.0,0.5],[5.95,0.5]]);
        polygon(points=[[6,1],[6.0,0.5],[5.95,0.5]]);            
		
        polygon(points=[[7,0],[7.0,0.5],[6.95,0.5]]);
        polygon(points=[[7,1],[7.0,0.5],[6.95,0.5]]);
		
        polygon(points=[[8,0],[8.0,0.5],[7.95,0.5]]);
        polygon(points=[[8,1],[8.0,0.5],[7.95,0.5]]);
		
        polygon(points=[[9,0],[9.0,0.5],[8.95,0.5]]);
        polygon(points=[[9,1],[9.0,0.5],[8.95,0.5]]);
		
        polygon(points=[[10,0],[10.0,0.5],[9.95,0.5]]);
        polygon(points=[[10,1],[10.0,0.5],[9.95,0.5]]);
		
//        polygon(points=[[11,0],[11.0,0.5],[10.95,0.5]]);
//        polygon(points=[[11,1],[11.0,0.5],[10.95,0.5]]);
		
//        polygon(points=[[12,0],[12.0,0.5],[11.95,0.5]]);
//        polygon(points=[[12,1],[12.0,0.5],[11.95,0.5]]);
		}       
   }

