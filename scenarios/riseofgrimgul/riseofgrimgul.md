# Rise of Grimgul
## Background
### Grimgul
Today, Grimgul the Wise is spoken of in terror. Part of the retinue of the Vampire Lord, his mighty raised armies are known for their britality at keeping invaders out of the Vampire Lord's realm.

But before he came into such company, and mellowed into his elder wisdom, Grimgul followed the path of most necromancers. A thirst for knowledge beyond the ability of his small village to sate, experimenting with things that he should not. His village began to shun him, which drove him further into the dark arts, seeking out knowledge from Durunjak, Dark God of Death. HIs experiments and dabblings were limited in scope, until he realised that to harness the energies of death, he needed to release them. So one one day he poisoned the well and killed half the village. The survivors all fled, fearing their village cursed, and they were not wrong. The fresh dead Grimgul was able to raise a few as zombies, as well as some corpses from the cemetary as skeletons. The village founder, a retired warrior of times gone by called Mikhael Jorgesson, was also raised as a Revenant. He gave command of his zombies to Mikhael so he could focus on the lore he studied. His small band of undead creatures was enough to protect him while he studied. 

A travelling adventurer from a neighbouring kingdom, Ran-lakk, wandered through the village and was horrified at what he saw. He took up arms against Grimgul, but despite his conviction he was no match for the powers Grimgul weilded. Toe to toe with Mikhael he was a better fighter, but every time he struck Mikhael down, Mikhael shrugged off the wounds and stood again. Eventually Ran-lakk staggered, exhaustion taking him, and Mikhael ran him through. Ran-lakk became Grimgul's next Revenant; he was given command of the skeletons that Grimgul had raised.

### The Blue Mountains
Soon after raising Ran-lakk, Grimgul's days (in which he slept, saving the nights for his research) were beset by strange dreams. In them Durunjak was but a minor demonling playing with dead animals, overshadowed by a looming presence of grey shadow, which Grimgul felt had the power over all of Pannithor's dead. The grey form was vast and powerful, yet try as he might Grimgul was unable to focus on it, and it remained a blurred fog. This looming presence gestured into the distance, unveiling an image of the Blue Mountains, urging Grimgul to go explore them.

Grimgul knew of the Blue Mountains, so called because of the faint glowing blue haze that cloaks them, day or night. The twisting gullies and valleys of the Blue Mountains were mostly inhabited by small villages, trading posts, hunting lodges and mines. Rumours of dark and evil things deep in the mountains were prevalent, but this was no different to any other hard to reach place in Pannithor, and most educated folk paid such rumours little heed.

Grimgul gathered his books and scrolls, and packed a rucksack full. Grabbing a walking staff, he set off with his retinue for the mountains.

The closer he got, the more frequent the dreams, and more specific in their exposition. Grimgul was now seeing specific trails and passes, and even a mine called Lightning Ridge where a vein of silver shot across the edge of the ravine.

He took a circuitous route, off the main trails, until he found the village of Nachtville. Leaving his retinue deep in the wilderness, he wandered in to the village and strode in to the local store. There he bought a map and asked about the surrounding area. The shopkeep was more than happy to explain the locality, and was even kind enough to mark on the map the "dark place", where it was best advised to never go, especially at night.

### The Barrow Opens
That night, Grimgul headed deep into the valleys and folds of the mountains, following the map to the dark place, which he was sure was where the dreams were leading him.

Hours passed, with the blue haze surrounding him and his undead followers. The paths were long and twisted, but eventually he began to hear a faint wailing, as if someone in torment. Following the sound, he eventually came upon a large, ancient barrow, from which the wailing emenated. Glyphs and runes etched into the stone had been crudely defaced with chips and scratches, desecrating the rites of interrment. Grimgul pushed open the rotten wooden door, and found a stone slab within. Upon the slab lay the bones of an ancient warrior of Korgaan, dead for thousands of years. Laying either side of the corpse were two blades, a short sword and a longsword, as clean as the day they were laid. Ancient steel faintly hummed with forgotten power.

Grimgul grasped the longer blade, and his head was filled with images and knowledge of unspeakable things. Working until dawn, he laid out the ritual he now knew, weaving foul new magics and his existing knowledge into a potent ressurrection incantation. As the sun rose over the peaks of the mountains, so too did his latest servant, Death Knell the Barrow Wight.

## Attack on Lightning Ridge
### Set up


### Scenario rules


### The winner


## Into the Weeds
### Set up


### Scenario rules


### The winner


## The Village in Peril
### Set up


### Scenario rules


### The winner


## Conclusion
### The winner

